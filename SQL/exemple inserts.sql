insert into personne (nom, prenom, date_naissance, taille) values ('chauvet', 'benoit', '1979-03-17', 175); -- 1
insert into personne (nom, prenom, date_naissance, taille) values ('ivanova', 'mishela', '1979-03-17', 175); -- 2
insert into personne (nom, prenom, date_naissance, taille) values ('veyssieres', 'christian', '1979-03-17', 175); -- 3

insert into voiture (immatriculation, id_proprietaire) values ('AB-800-DZ', 1); -- 1
insert into voiture (immatriculation, id_proprietaire) values ('XY-4545-CC', 1); -- 2
insert into voiture (immatriculation, id_proprietaire) values ('XX-984-EZ', 2); -- 3
insert into voiture (immatriculation, id_proprietaire) values ('TR-646-ED', null);



