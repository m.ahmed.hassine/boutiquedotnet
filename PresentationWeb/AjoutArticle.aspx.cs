﻿using AFCEPF.AI107.Boutique.Business;
using AFCEPF.AI107.Boutique.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AFCEPF.AI107.Boutique.Front
{
    public partial class AjoutArticle : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CatalogueBU bu = new CatalogueBU();
                List<RayonEntity> rayons = bu.GetListeRayons();
                rayons.Insert(0,new RayonEntity(-1, "Aucun"));

                ddlRayon.DataTextField = "Libelle";
                ddlRayon.DataValueField = "Id";
                ddlRayon.DataSource = rayons;
                ddlRayon.DataBind();
            }
        }

        protected void btnAjout_Click(object sender, EventArgs e)
        {
            ArticleEntity article = new ArticleEntity();
            article.Nom = txtNom.Text;
            article.Description = txtDescription.Text;
            float prix;
            float.TryParse(txtPrix.Text.Replace(".",","), out prix);
            article.PrixUnitaire = prix;
            int stock;
            int.TryParse(txtStock.Text, out stock);
            article.Stock = stock;
            if (ddlRayon.SelectedIndex > 0)
            {
                article.IdRayon = int.Parse(ddlRayon.SelectedValue);
            }
            CatalogueBU bu = new CatalogueBU();
            bu.AjouterArticle(article);

            lblMessage.Visible = true;
            lblMessage.CssClass += " success";
            lblMessage.Text = "Article ajouté avec succès";

            Response.Redirect("Accueil.aspx");
        }
    }
}