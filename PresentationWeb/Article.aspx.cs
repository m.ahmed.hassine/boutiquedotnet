﻿using AFCEPF.AI107.Boutique.Business;
using AFCEPF.AI107.Boutique.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AFCEPF.AI107.Boutique.Front
{
    public partial class Article : System.Web.UI.Page
    {
        private int idArticle;

        protected void Page_Load(object sender, EventArgs e)
        {
            // récupération de l'id article :
            int.TryParse(Request["id"], out idArticle);

            if (!IsPostBack)
            {
                // récupération dee l'entité article :
                CatalogueBU bu = new CatalogueBU();
                ArticleEntity article = bu.GetArticle(idArticle);

                // si j'ai récupéré un article, je l'affiche :
                if (article != null)
                {
                    imgPhoto.ImageUrl = "images/" + article.Photo;
                    imgPhoto.AlternateText = article.Nom;

                    lblTitre.Text = article.Nom;
                    lblDescription.Text = article.Description;
                    lblPrix.Text = article.PrixUnitaire.ToString();
                    lblStock.Text = article.Stock.ToString();
                }
            }
        }

        protected void btnSupprimer_Click(object sender, EventArgs e)
        {
            // récupérer l'id de l'article à supprimer : Déjà fait dans le page_load

            // demander la suppression au business
            CatalogueBU bu = new CatalogueBU();
            bu.SupprimerArticle(this.idArticle);

            // redirection vers la liste des prix
            Response.Redirect("PriceList.aspx");
        }
    }
}