﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="Recherche.aspx.cs" Inherits="AFCEPF.AI107.Boutique.Front.Recherche" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">

    <div>

        Nom : <asp:TextBox ID="txtNom" runat="server"></asp:TextBox>
        Rayon : <asp:DropDownList OnSelectedIndexChanged="ddlRayon_SelectedIndexChanged" AutoPostBack="true" ID="ddlRayon" runat="server"></asp:DropDownList>
        Prix max : <asp:TextBox ID="txtPrixMax" TextMode="Number" runat="server"></asp:TextBox>
        <asp:Button ID="btnRechercher" runat="server" OnClick="btnRechercher_Click" Text="Search" />
    </div>

    <asp:GridView ID="gvArticles" runat="server"></asp:GridView>

</asp:Content>
