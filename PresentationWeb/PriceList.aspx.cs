﻿using AFCEPF.AI107.Boutique.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AFCEPF.AI107.Boutique.Front
{
    public partial class PriceList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CatalogueBU bu = new CatalogueBU();

                gvArticles.DataSource = bu.GetArticleDetails();
                gvArticles.DataMember = "id_article";
                gvArticles.DataBind();


                rptArticles.DataSource = bu.GetArticleDetails();
                rptArticles.DataBind();
            }
        }

        // 3 - Codebehind de l'événement :
        protected void gvArticles_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if(e.CommandName == "plop")
            {
                Response.Write("plop" + e.CommandArgument);
            }

            if (e.CommandName == "plip")
            {
                Response.Write("plip" + e.CommandArgument);
            }
        }
    }
}