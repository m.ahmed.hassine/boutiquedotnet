﻿using AFCEPF.AI107.Boutique.Business;
using AFCEPF.AI107.Boutique.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AFCEPF.AI107.Boutique.Front
{
    public partial class EditionArticle : System.Web.UI.Page
    {
        private int idArticle;

        protected void Page_Load(object sender, EventArgs e)
        {
            // je récupère l'id de l'article :
            this.idArticle = int.Parse(Request["id"]);

            if (!IsPostBack)
            {
                // je remplis la liste des rayons :
                CatalogueBU bu = new CatalogueBU();
                List<RayonEntity> rayons = bu.GetListeRayons();
                rayons.Insert(0, new RayonEntity(-1, "Aucun"));

                ddlRayon.DataTextField = "Libelle";
                ddlRayon.DataValueField = "Id";
                ddlRayon.DataSource = rayons;
                ddlRayon.DataBind();

                ArticleEntity article = bu.GetArticle(this.idArticle);
                txtNom.Text = article.Nom;
                txtDescription.Text = article.Description;
                txtPrix.Text = article.PrixUnitaire.ToString();
                txtStock.Text = article.Stock.ToString();
                ddlRayon.SelectedValue = article.IdRayon.ToString();
            }

        }

        protected void btnEnregistrer_Click(object sender, EventArgs e)
        {
            // créer un articleEntity
            ArticleEntity article = new ArticleEntity();

            // lui donner les valeurs des champs saisis
            article.Nom = txtNom.Text;
            article.Description = txtDescription.Text;
            float prix;
            float.TryParse(txtPrix.Text.Replace(".", ","), out prix);
            article.PrixUnitaire = prix;
            int stock;
            int.TryParse(txtStock.Text, out stock);
            article.Stock = stock;
            if (ddlRayon.SelectedIndex > 0)
            {
                article.IdRayon = int.Parse(ddlRayon.SelectedValue);
            }
            
            // !! NE PAS OUBLIER L'id
            article.Id = this.idArticle;

            // Envoyer au business
            CatalogueBU bu = new CatalogueBU();
            bu.ModifierArticle(article);

            Response.Redirect("PriceList.aspx");
        }
    }
}