﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCSousMenuCatalogue.ascx.cs" Inherits="AFCEPF.AI107.Boutique.Front.UCSousMenuCatalogue" %>

<nav class="boite">
    <a href="PriceList.aspx">Liste des prix</a>
    | 
    <a href="AjoutArticle.aspx">Nouveau</a>
    |
    <asp:DropDownList ID="ddlArticles" runat="server"></asp:DropDownList>
    <asp:Button ID="btnDetails" runat="server" Text="Voir" OnClick="btnDetails_Click" />
</nav>
