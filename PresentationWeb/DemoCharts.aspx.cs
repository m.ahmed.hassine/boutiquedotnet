﻿using AFCEPF.AI107.Boutique.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.DataVisualization.Charting;

namespace AFCEPF.AI107.Boutique.Front
{
    public partial class DemoCharts : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CatalogueBU bu = new CatalogueBU();

            histo.DataSource = bu.GetListeArticles();
            histo.Series[0].XValueMember = "Nom";
            histo.Series[0].YValueMembers = "Stock";
            histo.DataBind();

            camembert.DataSource = bu.GetListeArticles();
            camembert.Series[0].ChartType = SeriesChartType.Doughnut;
            camembert.Series[0].XValueMember = "Nom";
            camembert.Series[0].YValueMembers = "Stock";
            camembert.DataBind();

            pile.DataSource = bu.GetListeArticles();
            pile.Series[0].ChartType = SeriesChartType.StackedColumn;
            pile.Series[0].XValueMember = "Nom";
            pile.Series[0].YValueMembers = "Stock";
            pile.Series[1].ChartType = SeriesChartType.StackedColumn;
            pile.Series[1].XValueMember = "Nom";
            pile.Series[1].YValueMembers = "PrixUnitaire";
            pile.DataBind();

        }
    }
}