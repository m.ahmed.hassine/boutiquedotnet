﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AFCEPF.AI107.Boutique.Front
{
    public partial class DemoDates : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // DATES :
            DateTime debut = new DateTime(2001,8,23);

            if (debut.DayOfWeek == DayOfWeek.Sunday)
            {
                Response.Write("c'est dimanche");
            }

            int annee = debut.Year;

            debut.AddDays(30);
            DateTime maintenant = DateTime.Now;
            debut = DateTime.MinValue;
            Response.Write(debut);

            // DUREES :
            TimeSpan duree = new TimeSpan(3, 12, 45, 18);

            int h = duree.Hours; // 12
            double th = duree.TotalHours; // 84,76

            // valeur choisie dans un composant calendar :
            //calDebut.SelectedDate

            txtDebut.Text = "27/06/2020";
            debut = DateTime.Parse(txtDebut.Text); 

            // date + duree = date
            DateTime dans3Jours = maintenant + duree;

            // date - date = duree
            TimeSpan duree2 = DateTime.Now - debut;

            // duree + duree = duree


        }
    }
}