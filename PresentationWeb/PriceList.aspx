﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="PriceList.aspx.cs" Inherits="AFCEPF.AI107.Boutique.Front.PriceList" %>

<%@ Register Src="~/UCSousMenuCatalogue.ascx" TagPrefix="uc1" TagName="UCSousMenuCatalogue" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">

    <uc1:UCSousMenuCatalogue runat="server" ID="UCSousMenuCatalogue" />

    <%-- 1 - Gestion des événements sur un gridview : OnRowCommand --%>
    <asp:GridView AutoGenerateColumns="false" ID="gvArticles" runat="server"
        OnRowCommand="gvArticles_RowCommand">
        <Columns>
            <asp:BoundField DataField="nom_article" HeaderText="Nom de l'article" />
            <asp:BoundField DataField="prix_unitaire" HeaderText="PU" />
            <asp:BoundField DataField="nom_rayon" HeaderText="Rayon" />
            <asp:HyperLinkField
                Text="Voir détails"
                DataNavigateUrlFormatString="Article.aspx?id={0}"
                DataNavigateUrlFields="id_article" />
            <asp:HyperLinkField
                Text="Editer"
                DataNavigateUrlFormatString="EditionArticle.aspx?id={0}"
                DataNavigateUrlFields="id_article" />
            <%-- 2- colonne de bouton : CommandName ET CommandArgument --%>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button ID="btnPlop" runat="server" 
                        CommandName="plop" 
                        CommandArgument='<%# Bind("id_article") %>' 
                        Text="test!" />
                    <asp:Button ID="btnPlip" runat="server" 
                        CommandName="plip" 
                        CommandArgument='<%# Bind("id_article") %>' 
                        Text="test!" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>





    <asp:Repeater ID="rptArticles" runat="server" Visible="false">
        <ItemTemplate>
            <div>
                <%# DataBinder.Eval(Container.DataItem, "nom_article") %>
            </div>
        </ItemTemplate>
    </asp:Repeater>

</asp:Content>
