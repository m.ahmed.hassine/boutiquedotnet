﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Boutique.Entities
{
    public class ArticleEntity
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public float? PrixUnitaire { get; set; }
        public string Description{ get; set; }
        public int Stock { get; set; }
        public string Photo { get; set; }
        public int? IdRayon { get; set; }
    }
}
