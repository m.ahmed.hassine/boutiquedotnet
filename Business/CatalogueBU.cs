﻿
using AFCEPF.AI107.Boutique.DataAccess;
using AFCEPF.AI107.Boutique.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Boutique.Business
{
    public class CatalogueBU
    {
        #region RAYONS
        public List<RayonEntity> GetListeRayons()
        {
            RayonDAO dao = new RayonDAO();
            return dao.GetAll();
        }

        public void AjouterRayon(RayonEntity nouveauRayon)
        {
            bool trouve = false;
            RayonDAO dao = new RayonDAO();
            // vérifier que le libellé est unique :
            foreach (RayonEntity r in dao.GetAll())
            {
                if (r.Libelle == nouveauRayon.Libelle)
                {
                    trouve = true;
                }
            }

            if (!trouve)
            {
                // inserer un rayon
                dao.Insert(nouveauRayon);
            }
            else
            {
                throw new Exception("ERREUR : Rayon déjà existant");
            }
        }
        #endregion

        #region ARTICLES

        public DataTable GetArticleDetails()
        {
            ArticleDAO dao = new ArticleDAO();
            return dao.GetDetailsRayon();
        }

        public DataTable RechercherArticles(string nom, int idRayon, float? prixMax)
        {
            ArticleDAO dao = new ArticleDAO();
            return dao.Rechercher(nom, idRayon, prixMax);
        }

        public ArticleEntity GetArticle(int id)
        {
            ArticleDAO dao = new ArticleDAO();
            return dao.GetById(id);
        }

        public List<ArticleEntity> GetListeArticles()
        {
            return GetListeArticles(-1);
        }

        public void SupprimerArticle(int idArticle)
        {
            ArticleDAO dao = new ArticleDAO();
            dao.Delete(idArticle);
        }

        public List<ArticleEntity> GetListeArticles(int idRayon)
        {
            ArticleDAO dao = new ArticleDAO();
            if (idRayon == -1)
            {
                return dao.GetAll();
            }
            else
            {
                return dao.GetByIdRayon(idRayon);
            }
        }

        public void AjouterArticle(ArticleEntity article)
        {
            ArticleDAO dao = new ArticleDAO();
            dao.Insert(article);
        }

        public void ModifierArticle(ArticleEntity article)
        {
            ArticleDAO dao = new ArticleDAO();
            dao.Update(article);
        }

        #endregion
    }
}
