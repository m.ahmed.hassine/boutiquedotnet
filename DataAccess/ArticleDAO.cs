﻿using AFCEPF.AI107.Boutique.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Boutique.DataAccess
{
    public class ArticleDAO : DAO
    {

        public DataTable GetDetailsRayon()
        {
            DataTable result = new DataTable();

            IDbCommand cmd = GetCommand(@"select 
                                          a.id 'id_article',
                                          a.nom 'nom_article', 
		                                  a.prix_unitaire, 
                                          r.libelle 'nom_rayon'
                                          from article a
                                          join rayon r on a.id_rayon = r.id;");

            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;

            // remplir la datatable :
            da.Fill(result);

            #region mode manuel
            // je crée la structure de ma datatable :
            //result.Columns.Add("nom_article", typeof(String));
            //result.Columns.Add("prix_unitaire", typeof(float));
            //result.Columns.Add("nom_rayon", typeof(String));
            

            //try
            //{
            //    // 3 - Ouvrir la connection
            //    cmd.Connection.Open();

            //    IDataReader dr = cmd.ExecuteReader();

            //    // lecture des resultats :
            //    while(dr.Read())
            //    {
                    //DataRow ligne = result.NewRow();
            //        ligne["nom_article"] = dr.GetString(dr.GetOrdinal("nom_article"));
            //        ligne["prix_unitaire"] = dr.GetFloat(dr.GetOrdinal("prix_unitaire"));
            //        ligne["nom_rayon"] = dr.GetString(dr.GetOrdinal("nom_rayon"));

            //        result.Rows.Add(ligne);
            //    }

            //}
            //catch (Exception exc)
            //{
            //    throw exc;
            //}
            //finally
            //{
            //    // 6 - Fermer la connection
            //    cmd.Connection.Close();
            //}
            #endregion

            return result;
        }

        // REQUETE MULTI CRITERES
        public DataTable Rechercher(string nom, int idRayon, float? prixMax)
        {
            DataTable result = new DataTable();

            IDbCommand cmd = GetCommand(@"SELECT 
                                          a.id 'id_article',
                                          a.nom 'nom_article', 
		                                  a.prix_unitaire, 
                                          r.libelle 'nom_rayon'
                                          FROM article a
                                          JOIN rayon r on a.id_rayon = r.id
                                          WHERE a.nom LIKE @nom
                                          AND (@idRayon = -1 OR a.id_rayon = @idRayon)
                                          -- AND (@idMarque = -1 OR a.id_marque = @idMarque)
                                          AND (@prixMax IS NULL OR a.prix_unitaire <= @prixMax)
                                          ORDER BY a.nom");

            cmd.Parameters.Add(new MySqlParameter("@nom", "%" + nom + "%"));
            cmd.Parameters.Add(new MySqlParameter("@idRayon", idRayon));
            cmd.Parameters.Add(new MySqlParameter("@prixMax", prixMax));

            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;

            // remplir la datatable :
            da.Fill(result);

            return result;
        }

        public void Delete(int idArticle)
        {
            IDbCommand cmd = GetCommand(@"DELETE FROM ARTICLE WHERE id = @id");

            cmd.Parameters.Add(new MySqlParameter("@id", idArticle));

            try
            {
                cmd.Connection.Open();

                cmd.ExecuteNonQuery();
            }
            catch(Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        public void Update(ArticleEntity article)
        {
            IDbCommand cmd = GetCommand(@"UPDATE article SET
                                                nom = @nom,
                                                description = @description,
                                                prix_unitaire = @prixUnitaire,
                                                stock = @stock,
                                                id_rayon = @idRayon
                                            WHERE id = @id");

            cmd.Parameters.Add(new MySqlParameter("@nom", article.Nom));
            cmd.Parameters.Add(new MySqlParameter("@description", article.Description));
            cmd.Parameters.Add(new MySqlParameter("@prixUnitaire", article.PrixUnitaire));
            cmd.Parameters.Add(new MySqlParameter("@stock", article.Stock));
            cmd.Parameters.Add(new MySqlParameter("@idRayon", article.IdRayon));
            cmd.Parameters.Add(new MySqlParameter("@id", article.Id));

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        public ArticleEntity GetById(int id)
        {
            ArticleEntity result = null;

            // 2 - Préparer une commande SQL
            IDbCommand cmd = GetCommand(@"SELECT * 
                                            FROM article
                                            WHERE id = @id");

            cmd.Parameters.Add(new MySqlParameter("@id", id));

            try
            {
                // 3 - Ouvrir la connection
                cmd.Connection.Open();

                // 4 - Exécuter la commande
                IDataReader dr = cmd.ExecuteReader();

                // 5 - Traiter le résultat
                if (dr.Read())
                {
                    result = DataReaderToEntity(dr);
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // 6 - Fermer la connection
                cmd.Connection.Close();
            }
            return result;
        }

        public List<ArticleEntity> GetAll()
        {
            List<ArticleEntity> result = new List<ArticleEntity>();

            // 2 - Préparer une commande SQL
            IDbCommand cmd = GetCommand(@"SELECT * 
                                            FROM article");

            try
            {
                // 3 - Ouvrir la connection
                cmd.Connection.Open();

                // 4 - Exécuter la commande
                IDataReader dr = cmd.ExecuteReader();

                // 5 - Traiter le résultat
                while (dr.Read())
                {
                    result.Add(DataReaderToEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // 6 - Fermer la connection
                cmd.Connection.Close();
            }
            return result;
        }

        public void Insert(ArticleEntity article)
        {
            IDbCommand cmd = GetCommand(@"INSERT INTO article
                    (nom, description, prix_unitaire, stock, id_rayon)
                    VALUES 
                    (@nom, @description, @prixUnitaire, @stock, @idRayon);
                    SELECT LAST_INSERT_ID();");

            cmd.Parameters.Add(new MySqlParameter("@nom", article.Nom));
            cmd.Parameters.Add(new MySqlParameter("@description", article.Description));
            cmd.Parameters.Add(new MySqlParameter("@prixUnitaire", article.PrixUnitaire));
            cmd.Parameters.Add(new MySqlParameter("@stock", article.Stock));
            cmd.Parameters.Add(new MySqlParameter("@idRayon", article.IdRayon));

            try
            {
                cmd.Connection.Open();
                article.Id = Convert.ToInt32(cmd.ExecuteScalar());
                //cmd.ExecuteNonQuery();
            }
            catch(Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        public List<ArticleEntity> GetByIdRayon(int idRayon)
        {
            List<ArticleEntity> result = new List<ArticleEntity>();

            // 2 - Préparer une commande SQL
            IDbCommand cmd = GetCommand(@"SELECT * 
                                            FROM article 
                                            WHERE id_rayon = @idRayon");

            cmd.Parameters.Add(new MySqlParameter("@idRayon", idRayon));

            try
            {
                // 3 - Ouvrir la connection
                cmd.Connection.Open();

                // 4 - Exécuter la commande
                IDataReader dr = cmd.ExecuteReader();

                // 5 - Traiter le résultat
                while (dr.Read())
                {
                    result.Add(DataReaderToEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // 6 - Fermer la connection
                cmd.Connection.Close();
            }
            return result;
        }


        private ArticleEntity DataReaderToEntity(IDataReader dr)
        {
            ArticleEntity article = new ArticleEntity();
            article.Id = dr.GetInt32(dr.GetOrdinal("id"));
            article.Nom = dr.GetString(dr.GetOrdinal("nom"));
            if (!dr.IsDBNull(dr.GetOrdinal("description")))
            {
                article.Description = dr.GetString(dr.GetOrdinal("description"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("prix_unitaire")))
            {
                article.PrixUnitaire = dr.GetFloat(dr.GetOrdinal("prix_unitaire"));
            }
            article.Stock = dr.GetInt32(dr.GetOrdinal("stock"));
            if (!dr.IsDBNull(dr.GetOrdinal("photo")))
            {
                article.Photo = dr.GetString(dr.GetOrdinal("photo"));
            }
            // Si un champ peut être null en base, il faut le vérifier 
            // AVANT de le lire 
            if (!dr.IsDBNull(dr.GetOrdinal("id_rayon")))
            {
                article.IdRayon = dr.GetInt32(dr.GetOrdinal("id_rayon"));
            }
            return article;
        }

    }
}
